/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leankit;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 *
 * @author ariel
 */
public class ClienteLeankit {
    String usr;
    String mail;
    String pass;
    
    /**
     * Constructor de la clase ClienteLeankit
     * @param usr nombre de usuario en leankit
     * @param mail mail con el cual creo su cuenta en leankit
     * @param pass  clave de la cuenta de leankit
     */
    ClienteLeankit(String usr, String mail, String pass){
        this.usr = usr;
        this.mail = mail;
        this.pass = pass;
    }
    /**
     * retorna un arreglo con los datos de todos los tableros pertenecientes al ClienteLeankit que llama al método
     * @return Board[]
     * @throws MalformedURLException
     * @throws IOException 
     */
    Board[] getBoards() throws MalformedURLException, IOException{
        IdBoard[] idBoards;
        Board[] boards;
        Gson gson = new Gson();
        String boardsURL = ".leankit.com/kanban/api/boards/";
        URL url = new URL("https://"+this.usr+boardsURL);
        URLConnection con = url.openConnection();
        final String Mail = this.mail;
        final String Pass = this.pass;
        Authenticator au = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(Mail, Pass.toCharArray());
            }
            
        };
        Authenticator.setDefault(au);
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String linea;
        String json = "";
        while((linea = in.readLine())!=null){
            json += linea;
        }
        
        replyData rep = gson.fromJson(json, replyData.class);
        idBoards = rep.ReplyData[0];
        boards = new Board[idBoards.length];
        for (int i = 0; i < idBoards.length; i++) {
            gson = new Gson();
            url = new URL("https://"+this.usr+boardsURL+idBoards[i].Id);
            con = url.openConnection();
            in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            json = "";
            while((linea = in.readLine()) != null){
                json += linea;
            }
            BoardReply breply = gson.fromJson(json, BoardReply.class);
            boards[i] = breply.ReplyData[0];
        }
        return boards;
    }
    
    /**
     * retorna los datos de un tablero pedido de manera explícita
     * @param idBoard identificador de un tablero perteneciente a un cliente
     * @return Board
     * @throws MalformedURLException
     * @throws IOException 
     */
    Board getBoard(int idBoard) throws MalformedURLException, IOException{
        Gson gson = new Gson();
        URL url = new URL("https://"+this.usr+".leankit.com/kanban/api/boards/"+idBoard);
        URLConnection con = url.openConnection();
        final String usuario = this.mail;
        final String clave = this.pass;
        Authenticator au = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication(usuario, clave.toCharArray());
            }
        };
        Authenticator.setDefault(au);
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String json = "";
        String linea;
        while((linea = in.readLine()) != null){
            json+=linea;
        }
        BoardReply breply = gson.fromJson(json, BoardReply.class);
        Board salida = breply.ReplyData[0];
        return salida;
    }
    
    /**
     * método que retorna la información de las columnas de un tablero específico
     * @param idBoard identificador de un tablero perteneciente a un cliente
     * @return Lane[]
     * @throws IOException 
     */
    Lane[] getLanes(int idBoard) throws IOException{
        Board boardData = this.getBoard(idBoard);
        Lane[] lanes = new Lane[boardData.Archive.length + boardData.Backlog.length + boardData.Lanes.length];
        int contador = 0;
        for (int i = 0; i < boardData.Backlog.length; i++) {
            lanes[contador] = boardData.Backlog[i];
            contador++;
        }
        for (int i = 0; i < boardData.Lanes.length; i++) {
            lanes[contador] = boardData.Lanes[i];
            contador++;
        }
        for (int i = 0; i < boardData.Archive.length; i++) {
            lanes[contador] = boardData.Archive[i];
            contador++;
        }
        return lanes;
    }
    
    /**
     * método que retorna una lista con los datos de todas las tarjetas de un tablero
     * @param idBoard identificador de un tablero perteneciente al usuario que castea el método
     * @return Card[]
     * @throws IOException 
     */
    Card[] getCards(int idBoard) throws IOException{
        Lane[] lanes = getLanes(idBoard);
        ArrayList<Integer> ids = new ArrayList<>();
        ArrayList<Card> trjs = new ArrayList<>();
        Card[] tarjetas;
        for (int i = 0; i < lanes.length; i++) {
            for (int j = 0; j < lanes[i].Cards.length; j++) {
                ids.add(lanes[i].Cards[j].Id);
            }
        }
        for (int i = 0; i < ids.size(); i++) {
            trjs.add(this.getCard(idBoard, ids.get(i)));
        }
        tarjetas = new Card[trjs.size()];
        for (int i = 0; i < tarjetas.length; i++) {
            tarjetas[i] = trjs.get(i);
        }
        return tarjetas;
    }
    
    /**
     * método que retorna los datos de una tarjeta específica
     * @param idBoard id de un tablero perteneciente al usuario
     * @param idCard id de la tarjeta a la que se debe acceder (la tarjeta debe pertenecer al tablero)
     * @return Card
     * @throws MalformedURLException
     * @throws IOException 
     */
    Card getCard(int idBoard, int idCard) throws MalformedURLException, IOException{
        URL url = new URL("https://"+this.usr+".leankit.com/kanban/api/board/"+idBoard+"/getcard/"+idCard);
        URLConnection con = url.openConnection();
        Gson gson = new Gson();
        final String usuario = this.mail;
        final String clave = this.pass;
        String json = "";
        String linea;
        Authenticator au = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(usuario, clave.toCharArray());
            }

        };
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        while((linea = in.readLine()) != null){
            json += linea;
        }
        CardReply creply = gson.fromJson(json, CardReply.class);
        return creply.ReplyData[0];
    }
}
