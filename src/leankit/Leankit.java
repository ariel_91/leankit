/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leankit;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.net.URLConnection;

/**
 *
 * @author ariel
 */
public class Leankit {

    /**
     * @param args the command line arguments
     * @throws java.net.MalformedURLException
     */
      
   
    
    public static void main(String[] args) throws MalformedURLException, IOException {
                
        //se debe crear una instancia de la clase ClienteLeankit, con los datos entregados
        ClienteLeankit client = new ClienteLeankit("ariel91", "ariel.merino@usach.cl", "ariel2206");
        
        //para obtener la información de todos los tableros se usa lo siguiente
        System.out.println("\n\ntableros:");
        Board[] tableros = client.getBoards();
        for (int i = 0; i < tableros.length; i++) {
            System.out.println("    "+tableros[i].Title);
        }
       
        //para obtener la información de un tablero en específico
        Board tablero = client.getBoard(tableros[1].Id); //tableros[1].Id corresponde a una id de tipo int
        System.out.println("\n\ntablero: "+tablero.Id+" "+tablero.Title);
        
        //para obtener la información de todas las tarjetas de un tablero
        System.out.println("\n\ntarjetas:");
        Card[] tarjetas = client.getCards(tablero.Id);
        for (int i = 0; i < tarjetas.length; i++) {
            System.out.println("    "+tarjetas[i].Id+" "+tarjetas[i].Title);
        }
        
        //para obtener la información de una tarjeta específica
        Card tarjeta = client.getCard(tablero.Id, tarjetas[0].Id);
        System.out.println("\n\ntarjeta: "+tarjeta.Id+" "+tarjeta.Title+" "+tarjeta.StartDate+" "+tarjeta.DueDate);
        
        //mostrar los usuarios de un tablero
        System.out.println("\n\nusuarios del tablero:");
        BoardUsrs[] usuarios = tablero.getBoardUsrs();
        for (int i = 0; i < usuarios.length; i++) {
            System.out.println("    "+usuarios[i].FullName+" "+usuarios[i].EmailAddress);
        }
        
        //obtener la información de las columnas de un tablero
        System.out.println("\n\ncolumnas:");
        Lane[] columnas = client.getLanes(tablero.Id);
        for (int i = 0; i < columnas.length; i++) {
            System.out.println("    "+columnas[i].Title+" "+columnas[i].ParentLaneId);
        }
        
        //obtener los ids y titulos de las tarjetas asociadas a una columna
        System.out.println("\n\ntarjetas de la columna:");
        CardId[] idTarjetas = columnas[3].getCards();
        for (int i = 0; i < idTarjetas.length; i++) {
            System.out.println("    "+idTarjetas[i].Id+" "+idTarjetas[i].Title);
        }
        
        //obtener los datos de los usuarios asignados a una tarjeta
        System.out.println("\n\nusuarios de la tarjeta:");
        BoardUsrs[] usuariosTarjeta = tarjeta.getUsers();
        for (int i = 0; i < usuariosTarjeta.length; i++) {
            System.out.println("    "+usuariosTarjeta[i].FullName+" "+usuariosTarjeta[i].EmailAddress);
        };
    }    
}