/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leankit;

/**
 *
 * @author ariel
 */
public class Card {
    int Id;
    int BoardId;
    int LaneId;
    int Priority;
    String Title;
    String BoardTitle;
    String LaneTitle;
    String Description;
    String PriorityText;
    String LastMove;
    String LastActivity;
    String CreateDate;
    String DueDate;
    String StartDate;
    BoardUsrs[] AssignedUsers;
    Card(){}
    /**
     * retorna una lista con los usuarios asignados a la tarjeta
     * @return BoardUsrs[]
     */
    BoardUsrs[] getUsers(){
        return this.AssignedUsers;
    }
}
