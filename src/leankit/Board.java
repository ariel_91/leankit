/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package leankit;

/**
 *
 * @author ariel
 */
public class Board {
    int Id;
    String Title;
    String Description;
    int OrganizationId;
    Lane[] Lanes;
    Lane[] Backlog;
    Lane[] Archive;
    BoardUsrs[] BoardUsers;
    Board(){}
    
    /**
     * método que retorna los datos relacionados a los usuarios del tablero
     * @return BoardUsrs
     */
    BoardUsrs[] getBoardUsrs(){
        return this.BoardUsers;
    }
}
